# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.modules import get_module_resource
from odoo.exceptions import ValidationError


_GENDER = [('1', 'Male'),
    ('2', 'Female')]

_CATEGORY = [('1', 'General'),
    ('2', 'Insurance'),
    ('3', 'Staff'),
    ('4', 'Student'),
    ('5', 'Government Insurance')]

_RELIGION = [('1', 'Muslim'),
    ('2', 'Christian'),
    ('3', 'Juche'),
    ('4', 'Other')]

_BLOODTYPE = [('1', '+O'),
    ('2', '-O'),
    ('3', '+A'),
    ('4', '-A'),
    ('5', '+B'),
    ('6', '-B'),
    ('7', '+AB'),
    ('8', '-AB')]

_MARITALSTATUS = [('1', 'Single'),
    ('2', 'Married'),
    ('3', 'Divorced'),
    ('4', 'Widowed')]

class res_partner(models.Model):
    _inherit = 'res.partner'
    
    @api.model
    def _get_default_sequence(self):
        return self.env['ir.sequence'].get('patient.emr')

    
    first_name = fields.Char(string='First Name', required= True, on_change="onchange_name(first_name)")
    second_name = fields.Char(string='Second Name',required =True, on_change="onchange_name(second_name)")
    third_name = fields.Char(string='Third Name',required = True, on_change="onchange_name(third_name)")
    last_name = fields.Char(string='Last Name', required = True, on_change="onchange_name(last_name)")
    birth_date = fields.Datetime(string="Birth Date")
    mobile_additional = fields.Char(string="Additional Mobile")
    gender = fields.Selection(selection=_GENDER)
    patient_category = fields.Selection(selection=_CATEGORY)
    religion = fields.Selection(selection=_RELIGION)
    blood_type = fields.Selection(selection=_BLOODTYPE)
    marital_status = fields.Selection(selection=_MARITALSTATUS)
    id_type = fields.Many2one('id.type',string = 'ID Type')
    id_number = fields.Char(string="ID number")
    patient_source = fields.Char(string="Patient Source")
    is_deceased = fields.Boolean(string = 'Is Death')
    date_death = fields.Datetime(string="Birth Date")
    age = fields.Char(string='Age',compute='_compute_age')
    relative_relation = fields.Many2one('relative.relation',string = 'Relation Relative')
    patient_sequence = fields.Char('Patient Number', size=32,
                       default=_get_default_sequence,
                       track_visibility='onchange')

    @api.multi
    def _compute_age(self):
        """ Age computed depending based on the birth date in the
         membership request.
        """
        now = datetime.now()
        for record in self:
            if record.birth_date:
                birthdate_date = fields.Datetime.from_string(
                    record.birth_date,
                )
                if record.is_deceased:
                    date_death = fields.Datetime.from_string(record.date_death)
                    delta = relativedelta(date_death, birthdate_date)
                    is_deceased = _(' (deceased)')
                else:
                    delta = relativedelta(now, birthdate_date)
                    is_deceased = ''
                years_months_days = '%d%s %d%s %d%s%s' % (
                    delta.years, _(' Years'), delta.months, _(' Months'),
                    delta.days, _(' Days'), is_deceased
                )
            else:
                years_months_days = _('No DoB')
            record.age = years_months_days
            
    @api.onchange('first_name','second_name','third_name','last_name')
    def onchange_name(self):
        if self.first_name:
            if self.second_name:
                if self.third_name:
                    if self.last_name: 
                        self.name = self.first_name + ' ' + str(self.second_name) + ' ' + str(self.third_name) + ' ' + str(self.last_name)
                    else:
                        self.name = self.first_name + ' ' + str(self.second_name) + ' ' + str(self.third_name)
                else:
                    self.name = self.first_name + ' ' + str(self.second_name)
            else:
                self.name = self.first_name
                
class id_type(models.Model):
    _name = 'id.type'
    
    name = fields.Char(string = 'Name',required = True)
    code = fields.Char(string= 'Code',required = True)
    
class relative_relation(models.Model):
    _name = 'relative.relation'
    
    name = fields.Char(string = 'Relation Name',required = True)
    code = fields.Char(string= 'Code',required = True)