# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class patient_statement(models.TransientModel):
    _name = "his.patient.statement"
    _description = "Patient Statement"

    patient_id = fields.Many2one('res.partner', string='Patient')
    date_from = fields.Datetime(string='Date From')
    date_to = fields.Datetime(string='Date To')