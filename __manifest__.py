# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'IDS HealthCare Management',
    'version': '1.0',
    'category': 'Billing',
    'sequence': 101,
    'summary': 'Medical',
    'description': "HealthCare Management Contains Billing Module with Billing Menu",
    'owner': 'Innovative Digital Solutions',
    'author': 'IDS :: Shady Ramadan Hassan',
    'website': 'http://www.ids-intl.com',
    'depends': ['account','account_voucher','base','mail','resource','web'],
    'data': [
        'views/account_invoice_view.xml',
        'views/account_voucher_views.xml',
        'views/account_payment_view.xml',
        'views/product_pricelist_views.xml',
        'views/patient_statement_view.xml',
        'views/billing_menu.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}